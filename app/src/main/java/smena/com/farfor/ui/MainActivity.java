package smena.com.farfor.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.SharedPreferences;
import android.util.Log;

import smena.com.farfor.ui.fragments.FragmentMain;

import smena.com.farfor.R;

public class MainActivity extends AppCompatActivity {

    private final FragmentManager fragmentManager = getFragmentManager();
    private final FragmentMain fragmentMain = new FragmentMain();;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.showMainFragment();
    }

    private void changeFragment(Fragment fragment)
    {
        fragmentManager.beginTransaction()
                .replace(R.id.MainActivity,fragment)
                .commit();
    }

    public void showMainFragment(){
        this.changeFragment(this.fragmentMain);
    }

    @Override
    public void onBackPressed() {
        this.fragmentMain.showCategorys(); //Костыль Надо сделать обработку в MainFragment.java
    }

}
