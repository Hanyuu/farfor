package smena.com.farfor.ui.fragments;

import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.AdapterView.OnItemSelectedListener;
import android.os.AsyncTask;
import android.content.SharedPreferences;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.io.InputStream;
import java.util.StringTokenizer;

import smena.com.farfor.R;
import smena.com.farfor.code.API;
import smena.com.farfor.code.Utils;
import smena.com.farfor.code.models.Category;

import java.io.File;
import java.net.URL;
import java.net.HttpURLConnection;

import android.net.http.HttpResponseCache;
import android.widget.Toast;

import smena.com.farfor.code.models.City;
import smena.com.farfor.code.models.IS08601;
import smena.com.farfor.code.models.Product;

import java.lang.reflect.Type;

import com.google.gson.reflect.TypeToken;
import com.google.gson.*;

public class FragmentMain extends DialogFragment implements OnItemSelectedListener {

    private Spinner spinner;
    private City current_city;
    private ArrayAdapter<City> adapter;
    private Context context;
    private TableLayout table;
    private SharedPreferences settings;
    private Gson gson = new Gson();
    private File cache;
    private final long cache_size = 1024 * 1024 * 20; // 20 MB

    //Показываем меню категорий. Используется в MainActivity на обработчеке onBackPressed
    public void showCategorys() {
        new AsyncGetCategorys().execute(current_city);
    }

    // Сохранение и загрузка кеша - начало. Название метода соответствует его задаче.
    public void saveCity(City city) {
        String json = this.gson.toJson(city);
        SharedPreferences.Editor editor = this.settings.edit();
        editor.putString("CurrentCity", json);
        editor.commit();
    }

    public City loadCurrentCity() {
        Log.d("Try load", "Cached current city");
        String json = settings.getString("CurrentCity", "");
        return gson.fromJson(json, City.class);
    }

    public HashMap<Integer, Category> loadCategorys(City city) {
        String json = settings.getString("CategorysCityId:" + city.getCity_id(), "");
        Type type = new TypeToken<HashMap<Integer, Category>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public void saveCategotys(City city, HashMap<Integer, Category> categorys) {
        String json = gson.toJson(categorys);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("CategorysCityId:" + city.getCity_id(), json);
        editor.commit();
    }

    public void saveCategoryProducts(Category category, List<Product> products) {
        String json = gson.toJson(products);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("ProductsCategoryId:" + category.getId(), json);
        editor.commit();
    }

    public List<Product> loadCategoryProducts(Category category) {
        String json = settings.getString("ProductsCategoryId:" + category.getId(), "");
        Type type = new TypeToken<List<Product>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public List<City> loadCities() {
        String json = settings.getString("Cities", "");
        Type type = new TypeToken<List<City>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public void saveCities(List<City> cities) {
        String json = gson.toJson(cities);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("Cities", json);
        editor.commit();
    }

    // Сохранение и загрузка кеша - конец
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        this.settings = this.getActivity().getPreferences(Context.MODE_PRIVATE);
        this.context = this.getActivity().getBaseContext();
        this.spinner = (Spinner) view.findViewById(R.id.select_city);
        this.table = (TableLayout) view.findViewById(R.id.main_table);
        this.adapter = new ArrayAdapter<>(this.getActivity(), android.R.layout.simple_spinner_dropdown_item, new ArrayList<City>());
        this.spinner.setAdapter(this.adapter);
        this.spinner.setOnItemSelectedListener(this);
        this.cache = new File(this.context.getCacheDir(), "img");
        try {
            HttpResponseCache.install(cache, cache_size);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this.context, "Cannot init HTTP Cache", Toast.LENGTH_SHORT).show();
        }
        new AsyncGetCities().execute();
        return view;
    }

    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

        current_city = adapter.getItem(position);
        current_city.setPosition(position);
        if (current_city.getName() != null) {
            Log.d("Selected City", current_city.getName());
            new AsyncGetCategorys().execute(current_city);
            saveCity(current_city);
        }

    }

    public void onNothingSelected(AdapterView<?> parent) {
    }
    // Todo: <b>ВСЕ</b> таски должны быть вынесены в соответствующие классы.
    // Todo: Также okHTTP имеет собственные средства реализации асинхронной загрузки, возможно стоит использовать их.

    // Таск для загрузки городов. Загружает Из кеша, если кешь пуст - загружает через АПИ и заполняет кешь.
    private class AsyncGetCities extends AsyncTask<String, Integer, List<City>> {

        @Override
        protected List<City> doInBackground(String... arg) {
            List<City> cities = loadCities();
            if (cities == null) {
                cities = API.getCities();
                Log.d("Loaded cities ", String.valueOf(cities.size()));
                saveCities(cities);
            } else {
                Log.d("Loaded cached cities ", String.valueOf(cities.size()));
            }
            return cities;
        }

        @Override
        protected void onPostExecute(List<City> cities) {
            super.onPostExecute(cities);

            adapter.addAll(cities);
            spinner.setAdapter(adapter);
            City city = loadCurrentCity();
            if (city != null && city.getPosition() != -1) {
                Log.d("Loaded cached city ", city.getName());
                spinner.setSelection(city.getPosition());
            }
        }
    }

    // Таск для загрузки категорий. Загружает Из кеша, если кешь пуст - загружает через АПИ и заполняет кешь.
    private class AsyncGetCategorys extends AsyncTask<City, Integer, HashMap<Integer, Category>> {

        private City city;
        private boolean loaded_from_cache = false;
        private ArrayList<Integer> updated_categorys;

        @Override
        protected HashMap<Integer, Category> doInBackground(City... arg) {
            this.city = arg[0];
            HashMap<Integer, Category> categorys;
            categorys = loadCategorys(this.city);
            if (categorys != null) {
                Log.d("loaded cached categorys", String.valueOf(categorys.size()));
                if (city.getLastUpdate() == null) {
                    city.setLastUpdate(IS08601.now());
                    saveCity(city);
                }
                this.updated_categorys = API.categotysToUpdate(this.city);
                this.loaded_from_cache = true;
                return categorys;
            } else {
                categorys = API.getCategorys(this.city);
                saveCategotys(this.city, categorys);
                city.setLastUpdate(IS08601.now());
                saveCity(city);
                Log.d("Loaded Categorys", String.valueOf(categorys.size()));
                this.loaded_from_cache = false;
                return categorys;
            }
        }

        @Override
        protected void onPostExecute(HashMap<Integer, Category> categorys) {
            super.onPostExecute(categorys);

            table.removeAllViews();
            boolean add;
            TableRow t = new TableRow(context);
            Category[] categorys_array = categorys.values().toArray(new Category[categorys.size()]);
            ;
            for (int i = 0; i < categorys.size(); i++) {
                Category category = categorys_array[i];
                TextView tv = new TextView(context);
                ImageView iv = new ImageView(context);
                new ImageLoadTask("http://farfor.ru" + category.getIcon_png_url(), iv).execute();
                // Шаманизм для отображения 2х позиций в одну строку.
                if (i % 2 == 0) {
                    t = new TableRow(context);
                    add = false;
                } else {
                    add = true;
                }
                //Если кол-во категорий не чётное (а не кто не обещал, что оно чётное) последнию тоже добавляем.
                //Да, костыль, да говно.
                if( i == categorys.size() - 1){
                    add = true;
                }

                //Если первая загрузка категорий, "загрузка не из кеша", то делаем кешь продуктов по каждой категории
                if (!this.loaded_from_cache) {
                    for (Category cat : categorys.values()) {
                        Log.d("Try creating cache", "For Category " + cat.getName());
                        new AsyncGetProducts().execute(this.city, cat, false);
                    }
                }
                //Обновляем кешь продуктов в категории, если её id в списке "обнавлённых"
                if (this.updated_categorys != null && this.updated_categorys.contains(category.getId())) {
                    Log.d("Updating Category ", category.getName());
                    new AsyncGetProducts().execute(this.city, category, false);
                }
                tv.setText(category.getName());
                t.addView(iv);
                t.addView(tv);
                tv.setOnClickListener((View view) -> {
                    new AsyncGetProducts().execute(this.city, category, true);
                });
                iv.setOnClickListener((View view) -> {
                    new AsyncGetProducts().execute(this.city, category, true);
                });

                if (add) {
                    table.addView(t);
                }

            }

        }
    }

    // Таск для загрузки позиций в категории. Загружает Из кеша, если кешь пуст - загружает через АПИ и заполняет кешь.
    private class AsyncGetProducts extends AsyncTask<Object, Integer, List<Product>> {

        private boolean show = false;

        @Override
        protected List<Product> doInBackground(Object... args) {
            Category category = (Category) args[1];
            List<Product> products;
            show = (Boolean) args[2];
            products = loadCategoryProducts(category);
            if (products != null) {
                Log.d("loaded cached products", String.valueOf(products.size()));
                return products;
            } else {
                products = API.getProducts((City) args[0], category);
                saveCategoryProducts(category, products);
                Log.d("Loaded products", String.valueOf(products.size()));
                return products;
            }
        }

        @Override
        protected void onPostExecute(List<Product> products) {
            super.onPostExecute(products);
            if (this.show) {
                table.removeAllViews();
                boolean add;
                TableRow t = new TableRow(context);
                for (int i = 0; i < products.size(); i++) {
                    // Шаманизм для отображения 2х позиций в одну строку.
                    if (i % 2 == 0) {
                        t = new TableRow(context);
                        add = false;
                    } else {
                        add = true;
                    }
                    //Если кол-во позиций в категории не чётное (а не кто не обещал, что оно чётное) последнию тоже добавляем.
                    //Да, костыль, да говно.
                    if( i == products.size() - 1){
                        add = true;
                    }
                    Product product = products.get(i);
                    TextView description = new TextView(context);
                    ImageView iv = new ImageView(context);
                    new ImageLoadTask("http://farfor.ru" + product.getList_image_url(), iv).execute();
                    //Склеиваем описание продукта. Название+Описание+Цена
                    //Для того, чтобы избежать распидорасивания таблицы при длинных описаниях - через каждые 30 символов втыкаем перенос строки.
                    description.setText(product.getName() + "\n" + Utils.addLinebreaks(product.getDescription(),30) + "\n" + product.getPrice() + "р.");
                    t.addView(iv);
                    t.addView(description);
                    if (add) {
                        table.addView(t);
                    }

                }
            }
        }
    }

    // Таск для загрузки картинок.
    private class ImageLoadTask extends AsyncTask<Void, Void, Bitmap> {

        private String url;
        private ImageView imageView;

        public ImageLoadTask(String url, ImageView imageView) {
            this.url = url;
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            try {
                URL urlConnection = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
                connection.addRequestProperty("Cache-Control", "max-stale=" + String.valueOf(60 * 60 * 24 * 28));
                connection.setUseCaches(true);
                InputStream input =  connection.getInputStream();
                Bitmap image = BitmapFactory.decodeStream(input);
                input.close();
                return image;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            if (result != null) {
                imageView.setImageBitmap(result);
            }
        }

    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        HttpResponseCache cache = HttpResponseCache.getInstalled();
        if(cache != null) {
            cache.flush();
        }
    }

}
