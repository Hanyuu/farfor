package smena.com.farfor.code;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import smena.com.farfor.code.models.Category;
import smena.com.farfor.code.models.City;
import smena.com.farfor.code.models.Product;
import android.content.SharedPreferences;

import org.json.*;
import java.util.ArrayList;
import java.util.HashMap;

public class API {
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    private static OkHttpClient client = new OkHttpClient();

    // Загрузка городов
    @Nullable
    public static ArrayList<City> getCities() {
        Request request = new Request.Builder().url("http://api.farfor.ru/v1/geo/cities/").build();
        try
        {
            Response response = client.newCall(request).execute();
            JSONArray cities = new JSONArray(response.body().string());
            ArrayList<City>  result = new ArrayList<>();
            result.add(new City()); //Empty first field
            for (int i=0; i < cities.length(); i++){
                JSONObject obj = cities.getJSONObject(i);
                City city = new City();
                city.setCity_id(obj.getInt("id"));
                city.setDomain_prefix(obj.getString("domain_prefix"));
                city.setName(obj.getString("name"));
                city.setType(obj.getString("type"));
                result.add(city);
            }
            return result;
        }catch(Exception e){
            return null;
        }
    }

    // Загрузка категорий по городу.
    @Nullable
    public static HashMap<Integer,Category> getCategorys(City city) {
        Request request = new Request.Builder().url(String.format("http://api.farfor.ru/v1/menu/categories/%s/%d",city.getType(),city.getCity_id())).build();

        try
        {
            Response response = client.newCall(request).execute();

            JSONArray categorys = new JSONArray(response.body().string());
            HashMap<Integer,Category>  result = new HashMap<>();
            for (int i=0; i < categorys.length(); i++){
                JSONObject obj = categorys.getJSONObject(i);
                Category category = new Category();

                category.setType(obj.getString("type"));
                category.setName(obj.getString("name"));
                category.setFor_elections_2016(obj.getBoolean("for_elections_2016"));
                category.setIcon_png_url(obj.getString("icon_png_url"));
                category.setIcon_url(obj.getString("icon_url"));
                //category.setImage_png_url(obj.getString("image_png_url"));
                category.setList_image_url(obj.getString("list_image_url"));
                category.setId(obj.getInt("id"));
                category.setIs_japan(obj.getBoolean("is_japan"));
                category.setUrl(obj.getString("url"));
                category.setSlug(obj.getString("slug"));


                result.put(category.getId(),category);
            }
            return result;
        }catch(Exception e){
            return null;
        }
    }

    // Загрузка позиций по категории.
    @Nullable
    public static ArrayList<Product> getProducts(City city, Category category) {
        Request request = new Request.Builder().url(String.format("http://api.farfor.ru/v1/menu/products/%s/%d/%s/%d",city.getType(),city.getCity_id(),category.getType(),category.getId())).build();
        try
        {
            Response response = client.newCall(request).execute();
            JSONArray cities = new JSONArray(response.body().string());
            ArrayList<Product>  result = new ArrayList<>();
            for (int i=0; i < cities.length(); i++){
                JSONObject obj = cities.getJSONObject(i);
                Product product = new Product();
                product.setName(obj.getString("name"));
                product.setSlug(obj.getString("slug"));
                product.setList_image_url(obj.getString("list_image_url"));
                product.setCategory_slug(obj.getString("category_slug"));
                product.setDescription(obj.getString("description"));
                product.setDrink(obj.getBoolean("isDrink"));
                JSONObject modifications = (JSONObject)obj.getJSONArray("modifications").get(0);
                product.setPrice(Float.parseFloat(modifications.get("price").toString()));
                result.add(product);
            }
            return result;
        }catch(Exception e){
            return null;
        }
    }

    //Загрузка ID'шников категорий которые обновились.
    @Nullable
    public static ArrayList<Integer> categotysToUpdate(City city){
        Request request = new Request.Builder().url(String.format("http://api.farfor.ru/v1/last-modified/common/%s/%d/?last_modified=%s",city.getType(),city.getCity_id(),city.getLastUpdate())).build();

        try {
            Response response = client.newCall(request).execute();
            ArrayList<Integer> result = new ArrayList<>();
            JSONObject data = new JSONObject(response.body().string());
            data = data.getJSONObject("need_to_update").getJSONObject("category_detail");
            for(int i=1;i<=data.length();i++){
                try {
                    if (data.getBoolean(String.valueOf(i))) {
                        result.add(i);
                    }
                }catch(JSONException ex){}
            }
            return result;
        }catch (Exception e){
            return null;
        }

    }
}

