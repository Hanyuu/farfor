package smena.com.farfor.code.models;

import java.util.Date;

/**
 * Created by oem on 26.05.17.
 */

public class City {
    private int city_id;
    private String domain_prefix;
    private String type;
    private String id;
    private String name;
    private int position = -1;
    private String lastUpdate = null;

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public String getDomain_prefix() {
        return domain_prefix;
    }

    public void setDomain_prefix(String domain_prefix) {
        this.domain_prefix = domain_prefix;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString(){
        return this.name;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
