package smena.com.farfor.code.models;

import java.util.Date;

/**
 * Created by oem on 26.05.17.
 */

public class Category {

    private String name;
    private String url;
    private String icon_url;
    private String icon_png_url;
    private String image_png_url;
    private String list_image_url;
    private String price_wiget;
    private String slug;
    private String type;
    private boolean is_japan;
    private boolean for_elections_2016;
    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIcon_url() {
        return icon_url;
    }

    public void setIcon_url(String icon_url) {
        this.icon_url = icon_url;
    }

    public String getIcon_png_url() {
        return icon_png_url;
    }

    public void setIcon_png_url(String icon_png_url) {
        this.icon_png_url = icon_png_url;
    }

    public String getImage_png_url() {
        return image_png_url;
    }

    public void setImage_png_url(String image_png_url) {
        this.image_png_url = image_png_url;
    }

    public String getList_image_url() {
        return list_image_url;
    }

    public void setList_image_url(String list_image_url) {
        this.list_image_url = list_image_url;
    }

    public String getPrice_wiget() {
        return price_wiget;
    }

    public void setPrice_wiget(String price_wiget) {
        this.price_wiget = price_wiget;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean is_japan() {
        return is_japan;
    }

    public void setIs_japan(boolean is_japan) {
        this.is_japan = is_japan;
    }

    public boolean isFor_elections_2016() {
        return for_elections_2016;
    }

    public void setFor_elections_2016(boolean for_elections_2016) {
        this.for_elections_2016 = for_elections_2016;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
