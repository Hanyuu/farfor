package smena.com.farfor.code.models;

/**
 * Created by oem on 26.05.17.
 */

public class Product {
    private String name;
    private String slug;
    private String description;
    private String list_image_url;
    private String category_slug;
    private int elements;
    private float price;
    private boolean isDrink;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getList_image_url() {
        return list_image_url;
    }

    public void setList_image_url(String list_image_url) {
        this.list_image_url = list_image_url;
    }

    public String getCategory_slug() {
        return category_slug;
    }

    public void setCategory_slug(String category_slug) {
        this.category_slug = category_slug;
    }

    public int getElements() {
        return elements;
    }

    public void setElements(int elements) {
        this.elements = elements;
    }

    public boolean isDrink() {
        return isDrink;
    }

    public void setDrink(boolean drink) {
        isDrink = drink;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
