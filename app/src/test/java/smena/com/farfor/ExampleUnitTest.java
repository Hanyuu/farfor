package smena.com.farfor;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;

import smena.com.farfor.code.API;
import smena.com.farfor.code.models.Category;
import smena.com.farfor.code.models.City;
import smena.com.farfor.code.models.IS08601;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void testAPI_GetCities() throws Exception {
        System.out.println(API.getCities().get(0).getName());
    }
    @Test
    public void testAPI_GetCategorys() throws Exception {
        City city = API.getCities().get(1);
        System.out.println(API.getCategorys(city));
    }
    @Test
    public void testAPI_GetProducts() throws Exception {
        City city = API.getCities().get(1);
        Category category = API.getCategorys(city).get(0);
        System.out.print(API.getProducts(city,category).get(0).getName());
    }
    @Test
    public void testAPI_categotysToUpdate() throws Exception {
        City city = API.getCities().get(1);
        city.setLastUpdate(IS08601.now());
        ArrayList<Integer> result = API.categotysToUpdate(city);
        System.out.print(result);
    }
}